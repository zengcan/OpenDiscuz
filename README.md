![mahua](http://www.discuz.net/static/image/common/logo.png)
##OpenDiscuz是什么?
一个响应式的discuz模板架构

##有哪些功能？

* 高端大气上档次的风格
    *  微软Metro标准
    *  兼容Bootstrap
* 响应式
* 支持瀑布流
* 完全开放的源代码

##有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

##捐助开发者
在兴趣的驱动下,写一个`免费`的东西，有欣喜，也还有汗水，希望你喜欢我的作品，同时也能支持一下。
当然，有钱捧个钱场（右上角的爱心标志，支持支付宝和PayPal捐助），没钱捧个人场，谢谢各位。

##感激
感谢以下的项目,排名不分先后

Bootstrap for Metro. 

##谁在用？
http://addon.discuz.com/?@20327.developer